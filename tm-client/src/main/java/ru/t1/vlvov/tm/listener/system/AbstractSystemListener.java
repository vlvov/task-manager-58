package ru.t1.vlvov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.vlvov.tm.api.service.ILoggerService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.Collection;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    private AbstractListener[] abstractListeners;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    protected ILoggerService loggerService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @NotNull
    protected Collection<AbstractListener> getListeners() {
        return Arrays.asList(abstractListeners);
    }

}
