package ru.t1.vlvov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
@Getter
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['server.port']}")
    private String port;

    @NotNull
    @Value("#{environment['server.host']}")
    private String host;

    @NotNull
    final String EMPTY_VALUE = "---";

}
