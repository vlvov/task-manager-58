package ru.t1.vlvov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.endpoint.*;
import ru.t1.vlvov.tm.api.service.*;
import ru.t1.vlvov.tm.api.service.dto.*;
import ru.t1.vlvov.tm.api.service.model.IProjectService;
import ru.t1.vlvov.tm.api.service.model.IUserService;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.endpoint.*;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.service.*;
import ru.t1.vlvov.tm.service.dto.*;
import ru.t1.vlvov.tm.service.model.ProjectService;
import ru.t1.vlvov.tm.service.model.UserService;
import ru.t1.vlvov.tm.util.SystemUtil;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.FileWriter;

@Component
@Getter
public final class Bootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private Backup backup;

    @NotNull
    @Autowired
    private AbstractEndpoint[] abstractEndpoints;

    private void registry(@NotNull final AbstractEndpoint[] abstractEndpoints) {
        if (abstractEndpoints == null) return;
        for (AbstractEndpoint endpoint : abstractEndpoints)
            registry(endpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task_manager.pid";
        final long pid = SystemUtil.getPID();
        File file = new File(fileName);
        FileWriter myWriter = new FileWriter(file);
        myWriter.write(Long.toString(pid));
        myWriter.close();
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("**TASK-MANAGER SERVER IS SHUTTING DOWN**");
        backup.stop();
    }

    public void run() {
        initPID();
        registry(abstractEndpoints);
        loggerService.info("**TASK-MANAGER SERVER STARTED**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}
