package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.vlvov.tm.api.service.dto.ISessionDtoService;
import ru.t1.vlvov.tm.dto.model.SessionDTO;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository> implements ISessionDtoService {

    @Autowired
    @NotNull
    private ISessionDtoRepository sessionRepositoryDTO;

    @Override
    @NotNull
    protected ISessionDtoRepository getRepository() {
        return context.getBean(ISessionDtoRepository.class);
    }
}
