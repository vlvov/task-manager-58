package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.api.service.dto.IUserDtoService;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.exception.entity.UserNotFoundException;
import ru.t1.vlvov.tm.exception.field.*;
import ru.t1.vlvov.tm.exception.user.ExistsLoginException;
import ru.t1.vlvov.tm.util.HashUtil;

import javax.persistence.EntityManager;

@Service
public final class UserDtoService extends AbstractDtoService<UserDTO, IUserDtoRepository> implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @NotNull
    protected IUserDtoRepository getRepository() {
        return context.getBean(IUserDtoRepository.class);
    }

    @Override
    @NotNull
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO model = new UserDTO();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        @NotNull final IUserDtoRepository userRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = userRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepositoryDTO.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO model = new UserDTO();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        model.setEmail(email);
        @NotNull final IUserDtoRepository userRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = userRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepositoryDTO.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final UserDTO model = new UserDTO();
        model.setLogin(login);
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        model.setRole(role);
        @NotNull final IUserDtoRepository userRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = userRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepositoryDTO.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository userRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = userRepositoryDTO.getEntityManager();
        try {
            return userRepositoryDTO.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final IUserDtoRepository userRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = userRepositoryDTO.getEntityManager();
        try {
            return userRepositoryDTO.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO model = findByLogin(login);
        if (model == null) throw new UserNotFoundException();
        remove(model);
        return model;
    }

    @Override
    @NotNull
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO model = findOneById(id);
        if (model == null) throw new UserNotFoundException();
        model.setPasswordHash(HashUtil.salt(password, propertyService));
        update(model);
        return model;
    }

    @Override
    @NotNull
    public UserDTO updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO model = findOneById(id);
        if (model == null) throw new UserNotFoundException();
        model.setFirstName(firstName);
        model.setLastName(lastName);
        model.setMiddleName(middleName);
        update(model);
        return model;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return findByEmail(email) != null;
    }

    @Override
    @NotNull
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO model = findByLogin(login);
        if (model == null) throw new UserNotFoundException();
        model.setLocked(true);
        update(model);
        return model;
    }

    @Override
    @NotNull
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO model = findByLogin(login);
        if (model == null) throw new UserNotFoundException();
        model.setLocked(false);
        update(model);
        return model;
    }

}
