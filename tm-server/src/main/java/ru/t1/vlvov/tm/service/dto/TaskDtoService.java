package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.vlvov.tm.api.service.dto.ITaskDtoService;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.exception.field.NameEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, ITaskDtoRepository> implements ITaskDtoService {

    @Override
    @NotNull
    protected ITaskDtoRepository getRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    @NotNull
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO model = new TaskDTO();
        model.setName(name);
        model.setUserId(userId);
        @NotNull final ITaskDtoRepository taskRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = taskRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepositoryDTO.add(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public TaskDTO create(@Nullable final String userId, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO model = new TaskDTO();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        @NotNull final ITaskDtoRepository taskRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = taskRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepositoryDTO.add(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskDtoRepository taskRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = taskRepositoryDTO.getEntityManager();
        try {
            return taskRepositoryDTO.findAllByProjectId(projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskDtoRepository taskRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = taskRepositoryDTO.getEntityManager();
        try {
            return taskRepositoryDTO.findAllByProjectId(projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        @NotNull final ITaskDtoRepository taskRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = taskRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepositoryDTO.update(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        @NotNull final ITaskDtoRepository taskRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = taskRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepositoryDTO.update(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
