package ru.t1.vlvov.tm.api.repository.model;

import ru.t1.vlvov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
