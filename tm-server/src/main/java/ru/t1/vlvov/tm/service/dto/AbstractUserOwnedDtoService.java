package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.vlvov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.vlvov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDtoRepository<M>> extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

    @NotNull
    protected abstract IUserOwnedDtoRepository<M> getRepository();

    @Override
    public void add(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repositoryDTO.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repositoryDTO.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repositoryDTO.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            return repositoryDTO.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable M findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            return repositoryDTO.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable String userId, @Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repositoryDTO.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        M model = findOneById(userId, id);
        remove(userId, model);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull final IUserOwnedDtoRepository<M> repositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = repositoryDTO.getEntityManager();
        try {
            return repositoryDTO.findAll(userId, sort);
        } finally {
            entityManager.close();
        }
    }

}
