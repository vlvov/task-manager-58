package ru.t1.vlvov.tm.exception.field;

public final class RoleEmptyException extends AbstractFieldNotFoundException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}